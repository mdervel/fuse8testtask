﻿using System;
using System.Collections.Generic;

namespace TestTask.Services
{
    /// <summary>
    /// Интерефейс для построения отчета по продажам
    /// </summary>
    public interface IReportService
    {
        /// <summary>
        /// Получить данные отчета по продажам за период
        /// </summary>
        /// <param name="beginDate">начало отчетного периода</param>
        /// <param name="endDate">конец отчетного периода</param>
        /// <returns></returns>
        IEnumerable<SaleReportItem> GetSaleReport(DateTime? beginDate, DateTime? endDate);
    }
}