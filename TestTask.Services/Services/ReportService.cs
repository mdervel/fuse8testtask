﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestTask.Store;

namespace TestTask.Services
{
    public class ReportService : IReportService
    {
        private readonly ITestTaskStoreDbContext _context;

        public ReportService(ITestTaskStoreDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SaleReportItem> GetSaleReport(DateTime? beginDate, DateTime? endDate)
        {
            var res =
                _context.Orders
                    .Where(x =>x.OrderDate >= (beginDate ?? DateTime.MinValue) && x.OrderDate <= (endDate ?? DateTime.MaxValue))
                    .Include(x => x.OrderDetails)
                    .SelectMany(x => x.OrderDetails.Select(d => new SaleReportItem()
                    {
                        OrderNum = x.ID,
                        OrderDate = x.OrderDate,
                        ProductName = d.Product.Name,
                        ProductNum = d.ProductID,
                        Quantity = d.Quantity ?? 0,
                        UnitPrice = d.UnitPrice ?? 0m
                    })).OrderBy(x=>x.OrderNum).ThenBy(x=>x.ProductNum).ToList();
            return res;

        }
    }
}
