using System;

namespace TestTask.Services
{
    public class SaleReportItem
    {
        public int OrderNum { get; set; }

        public DateTime OrderDate { get; set; }

        public int ProductNum { get; set; }

        public string ProductName { get; set; }

        public Int16 Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}