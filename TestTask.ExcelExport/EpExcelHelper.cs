﻿using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace TestTask.ExcelExport
{
    public static class EpExcelHelper
    {
        public static ExcelRange SetWrap(this ExcelRange range, bool value = true)
        {
            range.Style.WrapText = value;
            return range;
        }

        public static ExcelRange SetColor(this ExcelRange range, Color color)
        {
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(color);
            return range;
        }

        public static ExcelRange SetBorder(this ExcelRange range)
        {
            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            return range;
        }

        public static ExcelRange SetHorizontalAlignment(this ExcelRange range, ExcelHorizontalAlignment alignment)
        {
            range.Style.HorizontalAlignment = alignment;
            return range;
        }

        public static ExcelRange SetVerticalAlignment(this ExcelRange range, ExcelVerticalAlignment alignment)
        {
            range.Style.VerticalAlignment = alignment;
            return range;
        }

        public static ExcelRange SetMerge(this ExcelRange range)
        {
            range.Merge = true;
            return range;
        }

        public static ExcelRange SetValue(this ExcelRange range, object value)
        {
            range.Value = value;
            if (value is float || value is double || value is decimal)
            {
                range.Style.Numberformat.Format = "0.00";
            }
            return range;
        }

        public static ExcelRange SetValues(this ExcelRange range, int currentRow, int currentCol, params object[] values)
        {
            if (values == null || values.Length == 0)
                return range;
            for (int i = 0; i < values.Length; i++)
            {
                range[currentRow, currentCol + i].Value = values[i];
            }
            return range;
        }
        public static ExcelRange SetFormula(this ExcelRange range, string formula, bool isInteger = true)
        {
            range.Formula = formula;
            range.Calculate();
            if (!isInteger)
            {
                range.Style.Numberformat.Format = "0.00";
            }
            return range;
        }

        public static ExcelRange SetFontBold(this ExcelRange range)
        {
            range.Style.Font.Bold = true;
            return range;
        }

        public static ExcelRange SetFontSize(this ExcelRange range, float size)
        {
            range.Style.Font.Size = size;
            return range;
        }

        public static ExcelRange SetFontColor(this ExcelRange range, Color color)
        {
            range.Style.Font.Color.SetColor(color);
            return range;
        }

    }
}
