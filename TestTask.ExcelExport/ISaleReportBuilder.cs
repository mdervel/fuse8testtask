﻿using System.Collections.Generic;
using TestTask.Services;

namespace TestTask.ExcelExport
{
    /// <summary>
    /// Интерфейс для экспорта в Excel
    /// </summary>
    public interface ISaleReportBuilder
    {
        /// <summary>
        /// Указанный набор данных экспортирует в Excel. На выходе тело excel-документа
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        byte[] MakeExport(IEnumerable<SaleReportItem> data);
    }
}