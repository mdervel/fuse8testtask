﻿using System.Collections.Generic;
using System.Linq;
using TestTask.Services;

namespace TestTask.ExcelExport
{
    using System.IO;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    public class SaleReportBuilder : ISaleReportBuilder
    {
        private readonly object[] _headers = {
            "Номер заказа", "Дата заказа", "Артикул товара", "Название товара", "Количество реализованных единиц товара",
            "Цена реализации за единицу продукции", "Стоимость"
        };
        public byte[] MakeExport(IEnumerable<SaleReportItem> data)
        {
            var pck = new ExcelPackage();
            // создает страницу для нашиз данных
            var ws = pck.Workbook.Worksheets.Add("Основные данные отчета");
            var dataList = data.ToList();

            // выставляем стили для заголовка данных 
            ws.Cells[1, 1, 1, _headers.Length].SetHorizontalAlignment(ExcelHorizontalAlignment.Center)
                .SetVerticalAlignment(ExcelVerticalAlignment.Center)
                .SetFontBold()
                .SetBorder()
                .SetWrap();
            // и затем сами заголовки
            ws.Cells.SetValues(1,1,_headers);
            ws.Row(1).Height = 60;
            var currentRow = 2;
            foreach (var saleReportItem in dataList)
            {
                ws.Cells.SetValues(currentRow, 1, saleReportItem.OrderNum, saleReportItem.OrderDate.ToString("dd.MM.yyyy"),
                    saleReportItem.ProductNum, saleReportItem.ProductName, saleReportItem.Quantity,
                    saleReportItem.UnitPrice);
                ws.Cells[currentRow, _headers.Length].SetFormula($"=E{currentRow}*F{currentRow}");
                currentRow++;
            }
            // выравниваем шириину колонок по значениям
            for(int i = 1; i <= _headers.Length; i++)
	        {
                ws.Column(i).AutoFit();
            }

            byte[] result;
            using (var ms = new MemoryStream())
            {
                pck.SaveAs(ms);
                result = ms.ToArray();
            }
            
            return result;
        }

    }
}
