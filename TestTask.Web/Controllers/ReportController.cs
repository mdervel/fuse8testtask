﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TestTask.ExcelExport;
using TestTask.Services;
using TestTask.Web.ViewModels;

namespace TestTask.Web.Controllers
{
    public class ReportController : Controller
    {
        private readonly ISaleReportBuilder _saleReportBuilder;
        private readonly IReportService _reportService;
        private readonly IMapper _mapper;

        public ReportController(ISaleReportBuilder saleReportBuilder, IReportService reportService, IMapper mapper)
        {
            _saleReportBuilder = saleReportBuilder;
            _reportService = reportService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var viewModel = new SaleReportViewModel();

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Result(string beginDate, string endDate)
        {
            var viewModel = new SaleReportViewModel()
            {
                BeginDate = beginDate,
                EndDate = endDate,
                Items = _reportService.GetSaleReport(beginDate==null? (DateTime?)null: DateTime.Parse(beginDate), 
                                                    endDate == null ? (DateTime?)null : DateTime.Parse(endDate))
                        .Select(_mapper.Map<SaleReportItemViewModel>)
                        .ToList()
            };
            return View("Index", viewModel);
        }


        public ActionResult Export(string beginDate, string endDate)
        {
            var data = _reportService.GetSaleReport(beginDate == null ? (DateTime?)null : DateTime.Parse(beginDate), endDate == null ? (DateTime?)null : DateTime.Parse(endDate)).ToList();
            _saleReportBuilder.MakeExport(data);
            var caption = "Отчет по продажам";
            if (beginDate != null)
                caption += $" с {DateTime.Parse(beginDate):dd.MM.yyyy}";
            if (endDate != null)
                caption += $" по {DateTime.Parse(endDate):dd.MM.yyyy}";
            return File(_saleReportBuilder.MakeExport(data), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", caption + ".xlsx");
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
