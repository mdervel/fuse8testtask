﻿using System.Collections.Generic;

namespace TestTask.Web.ViewModels
{
    public class SaleReportViewModel
    {
        public SaleReportViewModel()
        {
            Items = new List<SaleReportItemViewModel>();
        }
        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public List<SaleReportItemViewModel> Items { get; set; }
    }
}