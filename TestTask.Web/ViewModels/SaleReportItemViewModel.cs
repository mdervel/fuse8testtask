﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Web.ViewModels
{
    public class SaleReportItemViewModel
    {
        public int OrderNum { get; set; }

        public string OrderDate { get; set; }

        public int ProductNum { get; set; }

        public string ProductName { get; set; }

        public Int16 Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
