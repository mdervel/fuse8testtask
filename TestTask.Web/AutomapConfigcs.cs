﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TestTask.Services;
using TestTask.Web.ViewModels;

namespace TestTask.Web
{
    public static class AutomapConfigcs
    {
        public static IMapperConfigurationExpression AddMapping(this IMapperConfigurationExpression configurationExpression)
        {
            configurationExpression.CreateMap<SaleReportItem, SaleReportItemViewModel>()
                .ForMember(x => x.OrderDate, o => o.MapFrom(src => src.OrderDate.ToString("dd.MM.yyyy")));

            return configurationExpression;
        }
    }
}
