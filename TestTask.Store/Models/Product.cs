using System.ComponentModel.DataAnnotations;

namespace TestTask.Store.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }
    }
}