﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestTask.Store.Models
{
    public class Order
    {
        [Key]
        public int ID { get; set; }

        public DateTime OrderDate { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
