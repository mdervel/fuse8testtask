﻿using TestTask.Store.Models;

namespace TestTask.Store
{
    using System;
    using System.Data.Entity;

    public class TestTaskStoreDbContext : DbContext, ITestTaskStoreDbContext
    {
        public TestTaskStoreDbContext() : base("SqlContext")
        {
        }

        public IDbSet<Order> Orders { get; set; }
        public IDbSet<OrderDetail> OrderDetails { get; set; }
        public IDbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException(nameof(modelBuilder));
            }
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<OrderDetail>().ToTable("OrderDetail");
            modelBuilder.Entity<Product>().ToTable("Product");

            base.OnModelCreating(modelBuilder);
        }
    }
}