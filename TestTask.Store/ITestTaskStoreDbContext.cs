using System;
using System.Data.Entity;
using TestTask.Store.Models;

namespace TestTask.Store
{
    public interface ITestTaskStoreDbContext : IDisposable
    {
        IDbSet<Order> Orders { get; set; }
        IDbSet<OrderDetail> OrderDetails { get; set; }
        IDbSet<Product> Products { get; set; }
    }
}